SKIP_SQUASH?=1
FRONTNAME=opsperator
IMAGE=opsperator/airsonic
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	SKIP_SQUASH=$(SKIP_SQUASH) TAG_ON_SUCCESS=$(TAG_ON_SUCCESS) \
	    TEST_MODE=true hack/build.sh

.PHONY: run
run:
	@@docker rm -f airsonic || true
	@@docker run --name airsonic \
		-p 4040:4040 \
		-e AIRSONIC_ADMIN_PASSWORD=secr3t \
		-e DO_NOT_RESTART=yep \
		-e DB_BACKEND=embed \
		-e DEBUG=toto \
		-it $(IMAGE)

.PHONY: demo
demo: run

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "APACHE_REPOSITORY_REF=$$BRANCH" \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "APACHE_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@if ! oc describe secret airsonic-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge:
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml | oc delete -f- || true
