---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    backups.opsperator.io/driver: postgres
    backups.opsperator.io/interval: daily
    backups.opsperator.io/preferredtime: "15"
    backups.opsperator.io/secret: airsonic-postgres-kube
    name: airsonic-postgres-kube
  name: airsonic-postgres-kube
  namespace: ci
spec:
  replicas: 1
  selector:
    matchLabels:
      name: airsonic-postgres-kube
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        name: airsonic-postgres-kube
    spec:
      containers:
      - env:
        - name: POSTGRESQL_ADMIN_PASSWORD
          valueFrom:
            secretKeyRef:
              key: database-admin-password
              name: airsonic-postgres-kube
        - name: POSTGRESQL_DATABASE
          valueFrom:
            secretKeyRef:
              key: database-name
              name: airsonic-postgres-kube
        - name: POSTGRESQL_MAX_CONNECTIONS
          value: "100"
        - name: POSTGRESQL_PASSWORD
          valueFrom:
            secretKeyRef:
              key: database-password
              name: airsonic-postgres-kube
        - name: POSTGRESQL_SHARED_BUFFERS
          value: 12MB
        - name: POSTGRESQL_UPGRADE
        - name: POSTGRESQL_USER
          valueFrom:
            secretKeyRef:
              key: database-user
              name: airsonic-postgres-kube
        - name: TZ
          value: Europe/Paris
        image: docker.io/centos/postgresql-12-centos7:latest
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - /bin/sh
            - -i
            - -c
            - pg_isready -h 127.0.0.1 -p 5432
          failureThreshold: 38
          initialDelaySeconds: 30
          periodSeconds: 15
          successThreshold: 1
          timeoutSeconds: 3
        name: postgres
        ports:
        - containerPort: 5432
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - /bin/sh
            - -i
            - -c
            - psql -h 127.0.0.1 -U $POSTGRESQL_USER -q -d $POSTGRESQL_DATABASE -c
              'SELECT 1'
          failureThreshold: 3
          initialDelaySeconds: 5
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            cpu: 300m
            memory: 512Mi
          requests:
            cpu: 300m
            memory: 512Mi
        volumeMounts:
        - mountPath: /var/lib/pgsql/data
          name: db
      - env:
        - name: DATA_SOURCE_USER
          valueFrom:
            secretKeyRef:
              key: database-user
              name: airsonic-postgres-kube
        - name: DATA_SOURCE_PASS
          valueFrom:
            secretKeyRef:
              key: database-password
              name: airsonic-postgres-kube
        - name: DATA_SOURCE_HOST
          value: 127.0.0.1
        - name: DATA_SOURCE_DBNAME
          valueFrom:
            secretKeyRef:
              key: database-name
              name: airsonic-postgres-kube
        - name: DEBUG
          value: yay
        image: registry.gitlab.com/synacksynack/opsperator/docker-pgexporter:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 38
          initialDelaySeconds: 30
          periodSeconds: 15
          successThreshold: 1
          tcpSocket:
            port: 9113
          timeoutSeconds: 3
        name: exporter
        ports:
        - containerPort: 9113
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 9113
            scheme: HTTP
          initialDelaySeconds: 5
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            cpu: 100m
            memory: 128Mi
          requests:
            cpu: 100m
            memory: 128Mi
      securityContext:
        runAsUser: 26
      volumes:
      - emptyDir: {}
        name: db
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    backups.opsperator.io/driver: ssh
    backups.opsperator.io/interval: daily
    backups.opsperator.io/preferredtime: "15"
    backups.opsperator.io/secret: backups-airsonic-kube
    backups.opsperator.io/service: backups-airsonic-kube
    name: airsonic-kube
  name: airsonic-kube
  namespace: ci
spec:
  replicas: 1
  selector:
    matchLabels:
      name: airsonic-kube
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        name: airsonic-kube
    spec:
      containers:
      - env:
        - name: AIRSONIC_ADMIN_PASSWORD
          valueFrom:
            secretKeyRef:
              key: admin-password
              name: airsonic-kube
        - name: DB_BACKEND
          value: postgres
        - name: JAVA_OPTIONS
          value: -Xms256m -Xmx256m
        - name: LANG
          value: en
        - name: ONLY_TRUST_KUBE_CA
          value: "true"
        - name: POSTGRES_DB
          valueFrom:
            secretKeyRef:
              key: database-name
              name: airsonic-postgres-kube
        - name: POSTGRES_HOST
          value: airsonic-postgres-kube
        - name: POSTGRES_PASSWORD
          valueFrom:
            secretKeyRef:
              key: database-password
              name: airsonic-postgres-kube
        - name: POSTGRES_USER
          valueFrom:
            secretKeyRef:
              key: database-user
              name: airsonic-postgres-kube
        - name: SMTP_HOST
          value: smtp.demo.local
        - name: SMTP_PORT
          value: "25"
        - name: THEME
          value: default
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-airsonic:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 39
          httpGet:
            path: /rest/ping
            port: 4040
            scheme: HTTP
          initialDelaySeconds: 30
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 8
        name: airsonic
        ports:
        - containerPort: 4040
          name: http
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /rest/ping
            port: 4040
            scheme: HTTP
          initialDelaySeconds: 5
          periodSeconds: 20
          successThreshold: 1
          timeoutSeconds: 5
        resources:
          limits:
            cpu: 700m
            memory: 1Gi
          requests:
            cpu: 10m
            memory: 768Mi
        volumeMounts:
        - mountPath: /airsonic/data
          name: data
          subPath: db
        - mountPath: /airsonic/music
          name: data
          subPath: music
        - mountPath: /airsonic/playlists
          name: data
          subPath: playlists
        - mountPath: /airsonic/podcasts
          name: data
          subPath: podcasts
      - image: registry.gitlab.com/synacksynack/opsperator/docker-sshd:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 3
          initialDelaySeconds: 30
          periodSeconds: 10
          successThreshold: 1
          tcpSocket:
            port: 2222
          timeoutSeconds: 3
        name: sshd
        ports:
        - containerPort: 2222
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          initialDelaySeconds: 30
          periodSeconds: 10
          successThreshold: 1
          tcpSocket:
            port: 2222
          timeoutSeconds: 1
        resources:
          limits:
            cpu: 100m
            memory: 512Mi
          requests:
            cpu: 10m
            memory: 128Mi
        volumeMounts:
        - mountPath: /.ssh/id_rsa.pub
          name: ssh
          subPath: id_rsa.pub
        - mountPath: /backups
          name: data
      volumes:
      - emptyDir: {}
        name: data
      - name: ssh
        secret:
          defaultMode: 420
          secretName: backups-airsonic-kube
