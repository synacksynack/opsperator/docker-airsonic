FROM opsperator/java

# Airsonic image for OpenShift Origin

ENV AIRSONIC_DIR=/airsonic \
    AIRSONIC_PORT=4040 \
    AIRSONIC_URL=https://github.com/airsonic-advanced/airsonic-advanced/releases/download \
    AIRSONIC_VERSION=11.0.0-SNAPSHOT.20220625052932 \
    CONTEXT_PATH=/ \
    DEBIAN_FRONTEND=noninteractive \
    UPNP_PORT=4041

LABEL io.k8s.description="Airsonic Advanced - music library" \
      io.k8s.display-name="Airsonic Advanced" \
      io.openshift.expose-services="4040:http,4041:upnp" \
      io.openshift.tags="airsonic,airsonic-advanced" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-airsonic" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$AIRSONIC_VERSION"

USER root
COPY config/* /

RUN set -x \
    && echo "# Install Dumb-init" \
    && apt-get -y update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && mkdir -p /usr/share/man/man1 /usr/share/man/man7 \
    && echo "# Install Airsonic-Advanced Dependencies" \
    && apt-get install --no-install-recommends -y ldap-utils openssl wget \
	mariadb-client postgresql-client \
    && apt-get install -y ffmpeg lame ca-certificates \
    && echo "# Install Airsonic-Advanced" \
    && for dir in data music playlists podcasts; \
	do \
	    mkdir -p "$AIRSONIC_DIR/$dir"; \
	done \
    && wget "$AIRSONIC_URL/$AIRSONIC_VERSION/airsonic.war" \
	-O "$AIRSONIC_DIR/airsonic.war" \
    && touch "$AIRSONIC_DIR/data/airsonic.properties" \
    && mv /reset-tls.sh /usr/local/bin/ \
    && echo "# Fixing permissions" \
    && for i in /etc/ssl /usr/local/share/ca-certificates; \
	do \
	    chown -R 1001:root "$i" \
	    && chmod -R g=u "$i"; \
	done \
    && chown -R 1001:root "$AIRSONIC_DIR" \
    && chmod -R g=u "$AIRSONIC_DIR" \
    && echo "# Cleaning Up" \
    && apt-get purge -y --auto-remove \
	-o APT::AutoRemove::RecommendsImportant=false \
    && apt-get -y remove --purge wget \
    && apt-get clean \
    && rm -rvf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
	/nsswrapper.sh \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
WORKDIR $AIRSONIC_DIR
ENTRYPOINT ["dumb-init", "--", "/run-airsonic.sh"]
