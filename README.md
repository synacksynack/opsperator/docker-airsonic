# k8s Airsonic-Advanced

Kubernetes Airsonic-Advanced image.

Build with:

```
$ make build
```

Test with:

```
$ make run
```

Build in OpenShift:

```
$ make ocbuild
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name              |    Description             | Default                                                             |
| :---------------------------- | -------------------------- | ------------------------------------------------------------------- |
|  `AIRSONIC_ADMIN_PASSWORD`    | Airsonic Admin Password    | `admin`                                                             |
|  `DB_BACKEND`                 | Airsonic DB Backend        | `jdbc` (`$AIRSONIC_DIR/data/db/airsonic.script`)                    |
|  `DO_NOT_RESTART`             | Provisioning Reset Toggle  | undef - provisioning my restart container (when `DB_BACKEND=embed`) |
|  `JAVA_OPTIONS`               | Airsonic JVM Options       | `-Xmx256m -Xms256m`                                                 |
|  `LANG`                       | Airsonic Locale Language   | `en`                                                                |
|  `MYSQL_DATABASE`             | MySQL Database Name        | `airsonic`                                                          |
|  `MYSQL_HOST`                 | MySQL Database Host        | `127.0.0.1`                                                         |
|  `MYSQL_PASSWORD`             | MySQL Database Password    | undef                                                               |
|  `MYSQL_PORT`                 | MySQL Database Port        | `3306`                                                              |
|  `MYSQL_USER`                 | MySQL Database Username    | `airsonic`                                                          |
|  `ONLY_TRUST_KUBE_CA`         | Don't trust base image CAs | `false`, any other value disables ca-certificates CAs               |
|  `OPENLDAP_BASE`              | OpenLDAP Base              | seds `OPENLDAP_DOMAIN`, `example.com` produces `dc=example,dc=com`  |
|  `OPENLDAP_DOMAIN`            | OpenLDAP Domain Name       | undef                                                               |
|  `OPENLDAP_HOST`              | OpenLDAP Backend Address   | undef                                                               |
|  `OPENLDAP_PORT`              | OpenLDAP Bind Port         | `389` or `636` depending on `OPENLDAP_PROTO`                        |
|  `OPENLDAP_PROTO`             | OpenLDAP Proto             | `ldap`                                                              |
|  `POSTGRES_DB`                | Postgres Database Name     | `airsonic`                                                          |
|  `POSTGRES_HOST`              | Postgres Database Host     | `127.0.0.1`                                                         |
|  `POSTGRES_PASSWORD`          | Postgres Database Password | undef                                                               |
|  `POSTGRES_PORT`              | Postgres Database Port     | `5432`                                                              |
|  `POSTGRES_USER`              | Postgres Database Username | `airsonic`                                                          |
|  `SMTP_ENC`                   | Airsonic SMTP Encryption   | `SSL/TLS` if `SMTP_PORT` is `465`, `StartTLS` or `None`             |
|  `SMTP_HOST`                  | Airsonic SMTP Relay        | undef                                                               |
|  `SMTP_PORT`                  | Airsonic SMTP Port         | `25`                                                                |
|  `THEME`                      | Airsonic Default Theme     | `default`                                                           |
|  `WAIT_BEFORE_RESTART`        | Wait Before Restart        | `0` - does not wait (when `DB_BACKEND=embed`)                       |
|  `WAIT_FOR_DB_INTERVAL`       | Provisioning Wait for DB   | `10` - checks for DB every 10 seconds (when `DB_BACKEND=embed`)     |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point    | Description                       |
| :--------------------- | --------------------------------- |
|  `/airsonic/data`      | Airsonic application database     |
|  `/airsonic/music`     | Airsonic music database           |
|  `/airsonic/playlists` | Airsonic playlists                |
|  `/airsonic/podcasts`  | Airsonic podcasts                 |
|  `/certs`              | Airsonic Certificate (optional)   |
