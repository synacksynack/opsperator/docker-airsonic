#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

AIRSONIC_ADMIN_PASSWORD="${AIRSONIC_ADMIN_PASSWORD:-secret42}"
DB_BACKEND=${DB_BACKEND:-jdbc}
WAIT_BEFORE_RESTART=${WAIT_BEFORE_RESTART:-0}
WAIT_FOR_DB_INTERVAL=${WAIT_FOR_DB_INTERVAL:-10}

cpt=0
if echo "$AIRSONIC_URL" | grep airsonic-advanced >/dev/null; then
    PWTABLE=user_credentials
    PWCOLUMN=credential
else
    PWTABLE=user
    PWCOLUMN=password
fi
if test "$DB_BACKEND" = postgres; then
    POSTGRES_DB=${POSTGRES_DB:-airsonic}
    POSTGRES_HOST=${POSTGRES_HOST:-127.0.0.1}
    POSTGRES_PASSWORD="${POSTGRES_PASSWORD:-secret}"
    POSTGRES_PORT=${POSTGRES_PORT:-5432}
    POSTGRES_USER=${POSTGRES_USER:-airsonic}
    echo Waiting for Postgres backend ...
    while true
    do
	if echo "SELECT * FROM \"$PWTABLE\"" | PGPASSWORD="$POSTGRES_PASSWORD" \
		psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB" | grep ' admin ' \
		>/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach Postgres" >&2
	    exit 1
	fi
	sleep 5
	echo Postgres is ... KO
	cpt=`expr $cpt + 1`
    done
    if ! grep "^$AIRSONIC_ADMIN_PASSWORD" \
	    /airsonic/data/old-admin-pw >/dev/null 2>&1; then
	if ! ( \
		    echo "UPDATE \"$PWTABLE\""; \
		    echo "SET $PWCOLUMN = '$AIRSONIC_ADMIN_PASSWORD'"; \
		    echo "WHERE username = 'admin'"; \
		) | PGPASSWORD="$POSTGRES_PASSWORD" \
		psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB"; then
	    echo WARNING: Failed updating admin password
	else
	    echo "$AIRSONIC_ADMIN_PASSWORD" >/airsonic/data/old-admin-pw
	fi
    fi
elif test "$DB_BACKEND" = mysql; then
    MYSQL_DATABASE=${MYSQL_DATABASE:-airsonic}
    MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
    MYSQL_PASSWORD="${MYSQL_PASSWORD:-secret}"
    MYSQL_PORT=${MYSQL_PORT:-3306}
    MYSQL_USER=${MYSQL_USER:-airsonic}
    echo Waiting for MySQL backend ...
    while true
    do
	if echo "SELECT * FROM $MYSQL_DATABASE.$PWTABLE" \
		| mysql -u "$MYSQL_USER" --password="$MYSQL_PASSWORD" \
		-h "$MYSQL_HOST" -P "$MYSQL_PORT" "$MYSQL_DATABASE" \
		| grep ' admin ' >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach MySQL" >&2
	    exit 1
	fi
	sleep 5
	echo MySQL is ... KO
	cpt=`expr $cpt + 1`
    done
    if ! grep "^$AIRSONIC_ADMIN_PASSWORD" \
	    /airsonic/data/old-admin-pw >/dev/null 2>&1; then
	if ! ( \
		    echo "UPDATE $MYSQL_DATABASE.$PWTABLE"; \
		    echo "SET $PWCOLUMN = '$AIRSONIC_ADMIN_PASSWORD'"; \
		    echo "WHERE username = 'admin'"; \
		) | mysql -u "$MYSQL_USER" \
		--password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
		-P "$MYSQL_PORT" "$MYSQL_DATABASE"; then
	    echo WARNING: Failed updating admin password
	else
	    echo "$AIRSONIC_ADMIN_PASSWORD" >/airsonic/data/old-admin-pw
	fi
    fi
else
    if ! test -s /airsonic/data/db/airsonic.script; then
	echo admin >/airsonic/data/old-admin-pw
    fi
    while ! test -s /airsonic/data/db/airsonic.script
    do
	sleep $WAIT_FOR_DB_INTERVAL
	echo Waiting for Airsonic DB to Initialize
    done
    sleep 20
    DO_RESTART=false
    if ! grep "^$AIRSONIC_ADMIN_PASSWORD" \
	    /airsonic/data/old-admin-pw >/dev/null 2>&1; then
	if echo "$AIRSONIC_URL" | grep airsonic-advanced >/dev/null; then
	    sed -i \
		"s|^INSERT INTO USER_CREDENTIALS VALUES('admin','admin','[^']*',|INSERT INTO USER_CREDENTIALS VALUES('admin','admin','$AIRSONIC_ADMIN_PASSWORD',|" \
		/airsonic/data/db/airsonic.log
	else
	    sed -i \
		"s|^INSERT INTO \"USER\" VALUES('admin','[^']*',|INSERT INTO \"USER\" VALUES('admin','$AIRSONIC_ADMIN_PASSWORD',|" \
		/airsonic/data/db/airsonic.script
	fi
	DO_RESTART=true
    fi
    if $DO_RESTART; then
	echo "$AIRSONIC_ADMIN_PASSWORD" >/airsonic/data/old-admin-pw
	echo NOTICE: Airsonic admin password reinitialized
	if test -z "$DO_NOT_RESTART"; then
	    if test "$WAIT_BEFORE_RESTART" -ge 0; then
		sleep $WAIT_BEFORE_RESTART
	    fi >/dev/null 2>&1
	    find /proc -name cmdline 2>/dev/null | grep -E '/proc/[0-9]*/cmdline' \
		| awk -F/ '{print $3}' | while read candidate
		do
		    if grep airsonic.war /proc/$candidate/cmdline >/dev/null 2>&1; then
			echo NOTICE: Reloading PID $candidate -- container will restart
			kill -HUP $candidate
		    fi
		done
	else
	    echo WARNING: Airsonic needs to be restarted
	fi
    fi
fi

exit 0
