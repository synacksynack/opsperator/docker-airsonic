#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

export RUNTIME_USER=airsonic
export RUNTIME_HOME=$AIRSONIC_DIR
. /usr/local/bin/nsswrapper.sh
. /usr/local/bin/reset-tls.sh

DB_BACKEND=${DB_BACKEND:-jdbc}
LANG=${LANG:-en}
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
THEME=${THEME:-default}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$JAVA_OPTIONS"; then
    JAVA_OPTIONS="-Xmx256m -Xms256m"
fi

mkdir -p $AIRSONIC_DIR/data/transcode
ln -fs /usr/bin/ffmpeg $AIRSONIC_DIR/data/transcode/ffmpeg
ln -fs /usr/bin/lame $AIRSONIC_DIR/data/transcode/lame

addSetting()
{
    local key value
    key="$1"
    shift
    value="$@"
    ##? is this really necessary
    ##  or does it get rewritten automatically?
    ##  to check on my existing (kvm/xen) ...
    value=`echo "$value" | sed 's|\([:=]\)|\\\\\1|g'`
    if grep "^$key=" "$AIRSONIC_DIR/data/airsonic.properties" >/dev/null 2>&1; then
	sed -i "s|^$key=.*$|$key=$value|" "$AIRSONIC_DIR/data/airsonic.properties"
    else
	echo "$key=$value" >>"$AIRSONIC_DIR/data/airsonic.properties"
    fi
}

if test -z "$WELCOME_MSG"; then
    WELCOME_MSG="Welcome to Airsonic"
fi
if test -z "$WELCOME_SUBTITLE"; then
    WELCOME_SUBTITLE="self-hosted music library"
fi
if test -z "$WELCOME_MSG2"; then
    WELCOME_MSG2="__Airsonic__"
fi

addSetting LocaleLanguage "$LANG"
addSetting GettingStartedEnabled false
addSetting IgnoredArticles "La Le Les The El Los Las"
addSetting Theme "$THEME"
addSetting WelcomeTitle "$WELCOME_MSG"
addSetting WelcomeSubtitle "$WELCOME_SUBTITLE"
addSetting WelcomeMessage2 "$WELCOME_MSG2"

if test "$OPENLDAP_HOST"; then
    addSetting LdapUrl "$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/ou=users,$OPENLDAP_BASE"
    addSetting LdapEnabled true
    addSetting LdapAutoShadowing true
    addSetting LdapSearchFilter "(uid\={0})"
else
    addSetting LdapEnabled false
fi
if test "$SMTP_HOST"; then
    SMTP_PORT=${SMTP_PORT:-25}
    if test -z "$SMTP_ENC"; then
	if test "$SMTP_PORT" = 465; then
	    SMTP_ENC=SSL/TLS
	else
	    SMTP_ENC=None
	fi
    fi
    addSetting SmtpServer $SMTP_HOST
    addSetting SmtpEncryption $SMTP_ENC
    addSetting SmtpPort $SMTP_PORT
    addSetting SmtpFrom info@$OPENLDAP_DOMAIN
fi

addSetting server.use-forward-headers true
addSetting IndexCreationInterval 1
addSetting IndexCreationHour 3
addSetting FastCacheEnabled false
addSetting OrganizeByFolderStructure true
addSetting IgnoreSymLinks false

if test "$DB_BACKEND" = postgres; then
    POSTGRES_DB=${POSTGRES_DB:-airsonic}
    POSTGRES_HOST=${POSTGRES_HOST:-127.0.0.1}
    POSTGRES_PASSWORD="${POSTGRES_PASSWORD:-secret}"
    POSTGRES_PORT=${POSTGRES_PORT:-5432}
    POSTGRES_USER=${POSTGRES_USER:-airsonic}
    echo Waiting for Postgres backend ...
    while true
    do
	if echo '\d' | PGPASSWORD="$POSTGRES_PASSWORD" \
		psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach Postgres" >&2
	    exit 1
	fi
	sleep 5
	echo Postgres is ... KO
	cpt=`expr $cpt + 1`
    done
    if ! grep '^DatabaseConfigEmbedUrl' \
	    "$AIRSONIC_DIR/data/airsonic.properties" >/dev/null; then
	addSetting DatabaseConfigType embed
	addSetting DatabaseConfigEmbedDriver org.postgresql.Driver
	addSetting DatabaseConfigEmbedUrl "jdbc:postgresql://$POSTGRES_HOST:$POSTGRES_PORT/$POSTGRES_DB?stringtype=unspecified"
	addSetting DatabaseConfigEmbedUsername "$POSTGRES_USER"
	addSetting DatabaseConfigEmbedPassword "$POSTGRES_PASSWORD"
	addSetting DatabaseUsertableQuote '"'
    fi
elif test "$DB_BACKEND" = mysql; then
    MYSQL_DATABASE=${MYSQL_DATABASE:-airsonic}
    MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
    MYSQL_PASSWORD="${MYSQL_PASSWORD:-secret}"
    MYSQL_PORT=${MYSQL_PORT:-3306}
    MYSQL_USER=${MYSQL_USER:-airsonic}
    echo Waiting for MySQL backend ...
    while true
    do
	if echo SHOW TABLES | mysql -u "$MYSQL_USER" \
		--password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
		-P "$MYSQL_PORT" "$MYSQL_DATABASE" >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach MySQL" >&2
	    exit 1
	fi
	sleep 5
	echo MySQL is ... KO
	cpt=`expr $cpt + 1`
    done
    if ! grep '^DatabaseConfigEmbedUrl' \
	    "$AIRSONIC_DIR/data/airsonic.properties" >/dev/null; then
	addSetting DatabaseConfigType embed
	addSetting DatabaseConfigEmbedDriver org.mariadb.jdbc.Driver
	addSetting DatabaseConfigEmbedUrl "jdbc:mariadb://$MYSQL_HOST:$MYSQL_PORT/$MYSQL_DATABASE"
	addSetting DatabaseConfigEmbedUsername "$MYSQL_USER"
	addSetting DatabaseConfigEmbedPassword "$MYSQL_PASSWORD"
    fi
elif test "$DB_BACKEND" = jdbc; then
    if test -s /airsonic/data/db/airsonic.script; then
	if echo "$AIRSONIC_URL" | grep airsonic-advanced >/dev/null; then
	    sed -i '/INSERT INTO DATABASECHANGELOGLOCK VALUES/d' \
		/airsonic/data/db/airsonic.log
	else
	    sed -i '/INSERT INTO DATABASECHANGELOGLOCK VALUES/d' \
		/airsonic/data/db/airsonic.script
	fi
	echo NOTICE: removed lock from database
    fi
    if ! grep '^DatabaseConfigEmbedUrl' \
	    "$AIRSONIC_DIR/data/airsonic.properties" >/dev/null; then
	addSetting DatabaseConfigType embed
	addSetting DatabaseConfigEmbedDriver org.hsqldb.jdbcDriver
	addSetting DatabaseConfigEmbedUrl "jdbc:hsqldb:file:$AIRSONIC_DIR/data/db/airsonic"
	addSetting DatabaseConfigEmbedUsername sa
	addSetting DatabaseConfigEmbedPassword ""
    fi
fi
/provision.sh &

exec java $JAVA_OPTIONS \
     -Dserver.host=0.0.0.0 \
     -Dserver.port=$AIRSONIC_PORT \
     -Dserver.contextPath=$CONTEXT_PATH \
     -Dairsonic.home=$AIRSONIC_DIR/data \
     "-Dairsonic.defaultMusicFolder=$AIRSONIC_DIR/music" \
     "-Dairsonic.defaultPodcastFolder=$AIRSONIC_DIR/podcasts" \
     "-Dairsonic.defaultPlaylistFolder=$AIRSONIC_DIR/playlists" \
     -DUPNP_PORT=$UPNP_PORT \
     -Djava.awt.headless=true \
     -jar airsonic.war "$@"
