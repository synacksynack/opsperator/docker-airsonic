#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/airsonic-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to www-data
	(
	    cat /etc/passwd
	    echo "airsonic:x:`id -u`:`id -g`:airsonic:$AIRSONIC_DIR:/sbin/nologin"
	) >/tmp/airsonic-passwd
	(
	    cat /etc/group
	    echo "airsonic:x:`id -g`:"
	) >/tmp/airsonic-group
    fi
    export NSS_WRAPPER_PASSWD=/tmp/airsonic-passwd
    export NSS_WRAPPER_GROUP=/tmp/airsonic-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
