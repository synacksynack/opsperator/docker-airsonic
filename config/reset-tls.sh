AIRSONIC_TRUST_STORE="${AIRSONIC_TRUST_STORE:-data/trusted.jks}"
AIRSONIC_TRUST_STORE_PASS="${AIRSONIC_TRUST_STORE_PASS:-changeit}"
ONLY_TRUST_KUBE_CA=${ONLY_TRUST_KUBE_CA:-false}

should_rehash=false
for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
do
    if test -s $f; then
	if ! test "$ONLY_TRUST_KUBE_CA" = false; then
	    if test -d /etc/pki/ca-trust/source/anchors; then
		if test -z "$had_reset"; then
		    rm -f /etc/pki/tls/cert.pem \
			/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
		    had_reset=true
		fi
		cat $f >>/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
		cat $f >>/etc/pki/tls/cert.pem
	    else
		if test -z "$had_reset"; then
		    find /etc/ssl/certs -maxdepth 1 -type f \! -name dhparam.pem -delete
		    had_reset=true
		fi
		d=`echo $f | sed 's|/|-|g'`
		if ! cat $f >/usr/local/share/ca-certificates/kube$d; then
		    echo WARNING: failed installing $f certificate authority >&2
		else
		    should_rehash=true
		fi
	    fi
	else
	    if test -d /etc/pki/ca-trust/source/anchors; then
		dir=/etc/pki/ca-trust/source/anchors
	    else
		dir=/usr/local/share/ca-certificates
	    fi
	    d=`echo $f | sed 's|/|-|g'`
	    if ! test -s $dir/kube$d; then
		if ! cat $f >$dir/kube$d; then
		    echo WARNING: failed installing $f certificate authority >&2
		else
		    should_rehash=true
		fi
	    fi
	fi
    fi
done

if $should_rehash; then
    if test -d /etc/pki/ca-trust/source/anchors; then
	if ! update-ca-trust; then
	    echo WARNING: failed updating trusted certificate authorities >&2
	fi
    elif test "$had_reset"; then
	if ! c_rehash /etc/ssl/certs; then
	    echo WARNING: failed updating trusted certificate authorities >&2
	fi
    elif ! update-ca-certificates; then
	echo WARNING: failed updating trusted certificate authorities >&2
    fi
fi
unset should_rehash had_reset dir

if ! test -s "$AIRSONIC_DIR/$AIRSONIC_TRUST_STORE"; then
    echo Provision Keystore
    if ! test -d "$(dirname "$AIRSONIC_DIR/$AIRSONIC_TRUST_STORE")"; then
	mkdir -p $(dirname "$AIRSONIC_DIR/$AIRSONIC_TRUST_STORE")
    fi
    if test -s /etc/ssl/certs/java/cacerts; then
	cp /etc/ssl/certs/java/cacerts "$AIRSONIC_DIR/$AIRSONIC_TRUST_STORE"
	rm /etc/ssl/certs/java/cacerts
	ln -sf "$AIRSONIC_DIR/$AIRSONIC_TRUST_STORE" /etc/ssl/certs/java/cacerts
    fi
    count=0
    for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
    do
	if ! test -s $f; then
	    continue
	fi
	old=0
	grep -n 'END CERTIFICATE' $f  | awk -F: '{print $1}' \
	    | while read stop
	    do
		count=`expr $count + 1`
		echo "Processing $f (#$count)"
		head -$stop $f | tail -`expr $stop - $old` >/tmp/insert.crt
		keytool -import -trustcacerts -alias inter$count \
		    -file /tmp/insert.crt \
		    -keystore "$AIRSONIC_DIR/$AIRSONIC_TRUST_STORE" \
		    -storepass "$AIRSONIC_TRUST_STORE_PASS" -noprompt
		old=$stop
	    done
	echo done with $f
    done
    rm -f /tmp/insert.crt
    unset count old
elif test -s /etc/ssl/certs/java/cacerts; then
    rm /etc/ssl/certs/java/cacerts
    ln -sf "$AIRSONIC_DIR/$AIRSONIC_TRUST_STORE" /etc/ssl/certs/java/cacerts
fi
unset CA_CHECK AIRSONIC_TRUST_STORE AIRSONIC_TRUST_STORE_PASS \
    ONLY_TRUST_KUBE_CA
